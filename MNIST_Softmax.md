

```python
#Importing tensorflow
import tensorflow as tf
```


```python
#Importing MNIST Dataset
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

#one hot vectors is 0 in most dimension except for one.
```

    Extracting MNIST_data/train-images-idx3-ubyte.gz
    Extracting MNIST_data/train-labels-idx1-ubyte.gz
    Extracting MNIST_data/t10k-images-idx3-ubyte.gz
    Extracting MNIST_data/t10k-labels-idx1-ubyte.gz
    


```python
#Creating placeholders

x = tf.placeholder(tf.float32, [None, 784])

#placeholder is not a variable. It is more of a value created for the tensorflow to run computation. 
#MNIST data is in 28*28 pixels which needs to be flattened into 784 dimension vector(28*28 = 784). Thus, a 2-D tensor is created
#with a shape of [None, 784](None means dimension can be of varied length).



```


```python
#Creating weights and biases
w = tf.Variable(tf.zeros([784,10]))
b = tf.Variable(tf.zeros([10]))

#initially w, b are zeros of (784,10) and (10) in dimension because it needs to learn the values so it does 
#not matter if the initial values are zeros. 
#our model is y = softmax((x*w) + b). Thus, w is of the dimension (784*10). Matrix multiplication of x and w is going 
#to result in a vector of shape (10). This can be added to b.  
```


```python
# our model
y = tf.nn.softmax(tf.matmul(x,w) + b)
```


```python
#training our model
y_ = tf.placeholder(tf.float32, [None,10])

#cross entropy function
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

#cross entropy function is defined in the description
#cross entropy function describes how inefficient our predictions are

```


```python
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

#gradient optimizer tries to move to that direction in the computation graph where the cost or loss is reduced.
#learning rate is 0.5

#init = tf.initialize_all_variables()
init = tf.global_variables_initializer()
```


```python
#interactive session:
sess = tf.Session()
sess.run(init)
```


```python
for i in range(100):
    batch_xs, batch_ys = mnist.train.next_batch(100)
    sess.run(train_step, feed_dict={x : batch_xs, y_: batch_ys})
```


```python
#Evaluating the model:
prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
```


```python
accuracy = tf.reduce_mean(tf.cast(prediction, tf.float32))
```


```python
print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
```

    0.8973
    
